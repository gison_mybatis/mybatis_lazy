package test.dao;

import com.gison.commons.mybatis.generic.GenericDao;
import org.springframework.stereotype.Repository;
import test.bean.User;

import java.util.List;

@Repository
public interface UserDao extends GenericDao<User, Integer> {
	public User selectByID(int id);
	public List<User> select();
	public int insert(User u);
	public int update(User u);
	public int delete(User u);
	
	public List<User> selectPage(User u);
	

}
