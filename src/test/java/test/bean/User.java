package test.bean;


import com.gison.commons.mybatis.annotation.Column;
import com.gison.commons.mybatis.annotation.Id;
import com.gison.commons.mybatis.annotation.Table;

@Table(name="user")
public class User{
	@Id
	private Integer id;
	private String name;
	@Column(name="user_name")
	private String userName;
	private String t2;
	private String t3;
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getT2() {
		return t2;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public void setT2(String t2) {
		this.t2 = t2;
	}
	public String getT3() {
		return t3;
	}
	public void setT3(String t3) {
		this.t3 = t3;
	}
	@Override
	public String toString() {
		return "User [id=" + id + ", name=" + name + ", userName=" + userName + ", t2="
				+ t2 + ", t3=" + t3 + "]";
	}
	
	
	
}
