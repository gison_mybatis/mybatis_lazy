package test.app;

import com.gison.commons.mybatis.query.Condition;
import com.gison.commons.mybatis.query.Criterion;
import com.gison.commons.mybatis.query.Pagination;
import com.gison.commons.mybatis.query.Paging;
import com.google.common.collect.Lists;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import test.bean.User;
import test.dao.UserDao;

import java.util.ArrayList;
import java.util.List;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:applicationContext-jdbc-test.xml" })
public class TestUser {

	@Autowired
	private UserDao userDao;
	
	@Test
	public void testSelect() {
//		System.out.println(userDao.selectFieldsAllEntitys(new String[]{"id","name"}));
//		List<Object> list =  new ArrayList<Object>();
//		list.add(1);
//		userDao.selectEntityBySql("select", list);
//		userDao.selectAllEntitys();
		// System.out.println( sampleDao.selectEntitysCountByCriterion(new
		// Criterion(Condition.EQ("id","1"))));
//		System.out.println(userDao.selectEntityById(1));;
		
//		System.out.println(userDao.selectFieldsEntityById(1, new String[]{"id","name"}));
		List<Integer> ids = new ArrayList<Integer>();
		ids.add(1);
		ids.add(2);
		System.out.println(userDao.selectEntityByIds(ids));
		System.out.println(userDao.selectFieldsEntityByIds(ids, new String[]{"id","name","userName"}));
		System.out.println(userDao.selectEntitysByCriterion(new Criterion(Condition.EQ("id", 1).or(Condition.EQ("name", "lin")))));
		
		System.out.println(userDao.selectEntityStringBySql("select name from user where id = ?", Lists.newArrayList(1)));
		
		Pagination<User> pagination = userDao.selectEntitysByPaging(new Criterion(Condition.EMPEY()), new Paging<User>(1, 3));
		System.out.println(pagination);
		User user = new User();
		user.setName("hgs");
		user.setUserName("hgs");
//		user.setId(15);
		userDao.insertEntity(user);
//		userDao.
	}

}
