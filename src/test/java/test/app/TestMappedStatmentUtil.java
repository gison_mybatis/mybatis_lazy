package test.app;

import com.gison.commons.mybatis.util.MappedStatmentUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:applicationContext-jdbc-test.xml" })
public class TestMappedStatmentUtil {

	@Test
	public void TesttransResourceToEntityClass() {
		MappedStatmentUtil.transResourceToEntityClass("net.wit.test.dao.UserDao.selectAllEntitys");

	}

}
