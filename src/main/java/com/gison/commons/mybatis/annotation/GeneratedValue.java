package com.gison.commons.mybatis.annotation;


import com.gison.commons.mybatis.enmu.GenerationOrder;
import com.gison.commons.mybatis.enmu.GenerationType;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;


@Target({ METHOD, FIELD })
@Retention(RUNTIME)
public @interface GeneratedValue {

	GenerationType strategy() default GenerationType.IDENTITY;
	
	GenerationOrder location() default GenerationOrder.BEFORE;

	String generator() default "";
}

