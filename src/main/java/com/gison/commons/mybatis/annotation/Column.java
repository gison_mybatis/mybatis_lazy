package com.gison.commons.mybatis.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target({ METHOD, FIELD })
@Retention(RUNTIME)
public @interface Column {

	/**
	 * 数据库字段名
	 */
	String name() default "";

	/**
	 * 避免查询时字段为数据库的关键字，为true 拼接sql时会在字段的前后加  `
	 */
	boolean escape() default false;

	

}