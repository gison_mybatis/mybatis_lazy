package com.gison.commons.mybatis.generic;


import com.gison.commons.mybatis.query.Criterion;
import com.gison.commons.mybatis.query.Pagination;
import com.gison.commons.mybatis.query.Paging;
import org.apache.ibatis.annotations.*;

import java.util.List;

public interface GenericDao<Entity, ID> {
	
	@Select("Generic.entity.select.all")
	public List<Entity> selectAllEntitys();
	
	@Select("Generic.entity.select.fields.all")
	public List<Entity> selectFieldsAllEntitys(String[] fileds);
	
	@Select("Generic.entity.select.all.count")
	public Long selectAllEntitysCount();
	
	@Select("Generic.entity.select.id")
	public Entity selectEntityById(ID id);
	
	@Select("Generic.entity.select.fields.id")
	public Entity selectFieldsEntityById(@Param("id") ID id, @Param("fileds") String[] fileds);

	@Select("Generic.entity.select.ids")
	public List<Entity> selectEntityByIds(List<ID> list);

	@Select("Generic.entity.select.fields.ids")
	public List<Entity> selectFieldsEntityByIds(@Param("list") List<ID> list, @Param("fields") String[] fields);

	@Select("Generic.entity.select.condition.unique")
	public Entity selectEntityByCriterion(Criterion criterion);

	@Select("Generic.entity.select.condition.multiple")
	public List<Entity> selectEntitysByCriterion(Criterion criterion);

	@Select("Generic.entity.select.condition.count")
	public Long selectEntitysCountByCriterion(Criterion criterion);

	@Select("Generic.entity.select.sql.single")
	public Entity selectEntityBySql(@Param("sql") String sql, @Param("list") List<?> params);

	@Select("Generic.entity.select.sql.number")
	public Long selectEntityNumberBySql(@Param("sql") String sql, @Param("list") List<?> params);

	@Select("Generic.entity.select.sql.string")
	public String selectEntityStringBySql(@Param("sql") String sql, @Param("list") List<?> params);

	@Select("Generic.entity.select.sql.multiple")
	public List<Entity> selectEntitysBySql(@Param("sql") String sql, @Param("list") List<?> params);

	@Select("Generic.entity.select.sql.count")
	public Long selectEntitysCountBySql(@Param("sql") String sql, @Param("list") List<?> params);

	@Select("Generic.entity.selects.condition.paging")
	public Pagination<Entity> selectEntitysByPaging(@Param("condition") Criterion condition, @Param("paging") Paging<Entity> paging);
	
	@Insert("Generic.entity.insert.single")
	public Integer insertEntity(Entity entity);
	
	@Insert("Generic.entity.replace.single")
	public Integer replaceEntity(Entity entity);
	
	@Insert("Generic.entity.insert.multiple")
	public Integer insertEntitys(List<Entity> entitys);
	
	@Insert("Generic.entity.insert.sql")
	public Integer insertEntityBySql(@Param("sql") String sql, @Param("list") List<?> params);

	 
	@Update("Generic.entity.update.single")
	public Integer updateEntity(Entity entity);


	@Update("Generic.entity.update.notnul")
	public Integer updateEntityNotNul(Entity entity);


	@Update("Generic.entity.update.single.criterion")
	public Integer updateEntityByCriterion(Entity entity,Criterion criterion);

	@Update("Generic.entity.update.notnul.criterion")
	public Integer updateEntityNotNulByCriterion(Entity entity,Criterion criterion);

	@Update("Generic.entity.update.sql")
	public Integer updateEntityBySql(@Param("sql") String sql, @Param("list") List<?> params);

	@Deprecated
	@Delete("Generic.entity.delete.single")
	public Integer deleteEntity(Entity entity);

	@Deprecated
	@Delete("Generic.entity.delete.multiple")
	public Integer deleteEntitys(List<Entity> entitys);

	@Delete("Generic.entity.delete.id")
	public Integer deleteEntityById(ID id);

	@Delete("Generic.entity.delete.ids")
	public Integer deleteEntityByIds(List<ID> ids);

	@Delete("Generic.entity.delete.sql")
	public Integer deleteEntityBySql(@Param("sql") String sql, @Param("list") List<?> params);

	 
	@Delete("Generic.entity.delete.condition")
	public Integer deleteEntitysByCriterion(Criterion criterion);
}
