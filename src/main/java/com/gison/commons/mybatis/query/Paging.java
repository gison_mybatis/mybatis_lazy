package com.gison.commons.mybatis.query;



public class Paging<T> {

	public static final String PAGING = "paging";
	private static final Integer PAGING_DEFAULT_PAGE = 1;
	private static final Integer PAGING_DEFAULT_SIZE = 10;

	private int page;
	private int size;

	public Paging() {
		this.page=PAGING_DEFAULT_PAGE;
		this.size=PAGING_DEFAULT_SIZE;
	}

	public Paging(int page, int size) {
		this.page = page;
		this.size = size;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public int getFrom() {
		return (this.page - 1) * this.size;
	}

}
