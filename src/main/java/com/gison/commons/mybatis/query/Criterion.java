package com.gison.commons.mybatis.query;

import java.util.List;

public class Criterion {
	public final static String ASC = "ASC";
	public final static String DESC = "DESC";
	private StringBuffer orderBys = new StringBuffer();
	private Condition condition = null;
	
	public Criterion(){
		this(null);
	}
	
	public Criterion(Condition condition) {
		this.condition = condition != null ? condition : new Condition();
	}
	
	public List<Object> getParams() {
		return this.condition.getParams();
	}
	
	public String toSQL() {
		String whereConndition = "";
		if (condition.getBuffer().length() != 0) {
			whereConndition = " WHERE " + condition.getBuffer().toString();
		}
		if (orderBys.length() == 0) {
			return whereConndition;
		}
		return orderBys.length() == 0 ? whereConndition : whereConndition + " ORDER BY " + orderBys.substring(1);
	}

	public Criterion orderBy(String field, String order) {
		orderBys.append(",").append("`").append(field).append("`").append(" ").append(order);
		return this;
	}
	
public static void main(String[] args) {
		
		String keyword="11";
		Integer types[]={1,2};
		Condition condition1=Condition.LIKE("username", keyword, "%","%");
		Condition condition2=Condition.EMPEY();
		Condition condition3=Condition.IN("type",(Object[])types);
	//	condition2.or(Condition.EQ("type1", 1),Condition.EQ("type2", 1),Condition.EQ("type3", 2)).or(Condition.EQ("type4", 2));
		condition2.or(Condition.EQ("type1", null))
		.or(Condition.EQ("type2", null))
		.or(Condition.EQ("type3", 1))
		.or(Condition.EQ("type4", 2))
		;
	///	 WHERE (((type1 = ? or type2 = ?) or type3 = ?) or type4 = ?) ORDER BY username DESC,aaab ASC,aaac DESC
	//	 WHERE (((type1 = ? or type2 = ?) or type3 = ?) or type4 = ?) ORDER BY username DESC,aaab ASC,aaac DESC
		
		//condition3=Condition.IN("name", "a","b");
		Criterion criterion=new Criterion(Condition.AND(condition1,condition2,condition3)).orderBy("username", Criterion.DESC);
		//Criterion criterion=new Criterion(Condition.SQL("ssss=?","aa","b")).orderBy("username", Criterion.DESC);
		System.out.println(criterion.orderBy("aaab", Criterion.ASC).orderBy("aaac", Criterion.DESC).toSQL());
		
		System.out.println(condition3.toCriterion().orderBy("aaab", Criterion.ASC).orderBy("aaac", Criterion.DESC).toSQL());
	}
}
