package com.gison.commons.mybatis.query;

public class QueryField {
	private String[] fieldArray;
	
	public QueryField(){
		
	}
	
	public QueryField(String ...fields){
		fieldArray = fields;
	}
	
//	public static QueryField add(String field){
//		fieldArray
//	}

	public String[] getFieldArray() {
		return fieldArray;
	}

	public void setFieldArray(String[] fieldArray) {
		this.fieldArray = fieldArray;
	}
}
