package com.gison.commons.mybatis.query;


import com.gison.commons.mybatis.enmu.GenerationOrder;
import com.gison.commons.mybatis.enmu.GenerationType;

public class Parameter {
	private String field;
	private Object value;

	private Class<?> javaType;
	private String generator;
	private String keyColumn;
	private String keyProperty;
	private boolean generated;
	private GenerationType strategy;
	private GenerationOrder location;

	public Parameter() {
	}

	public Parameter(String field, Object value) {
		this.field = field;
		this.value = value;
	}

	public Parameter(String field, Object value, String keyColumn, String keyProperty, GenerationType strategy,
			String generator, GenerationOrder location, boolean generated, Class<?> javaType) {
		this.field = field;
		this.value = value;

		this.javaType = javaType;
		this.strategy = strategy;
		this.location = location;
		this.generated = generated;
		this.generator = generator;
		this.keyColumn = keyColumn;
		this.keyProperty = keyProperty;
	}

	public boolean isGenerated() {
		return generated;
	}

	public void setGenerated(boolean generated) {
		this.generated = generated;
	}

	public String getField() {
		return field;
	}

	public void setField(String field) {
		this.field = field;
	}

	public GenerationOrder getLocation() {
		return location;
	}

	public void setLocation(GenerationOrder location) {
		this.location = location;
	}

	public Object getValue() {
		return value;
	}

	public void setValue(Object value) {
		this.value = value;
	}

	public String getKeyColumn() {
		return keyColumn;
	}

	public void setKeyColumn(String keyColumn) {
		this.keyColumn = keyColumn;
	}

	public String getKeyProperty() {
		return keyProperty;
	}

	public void setKeyProperty(String keyProperty) {
		this.keyProperty = keyProperty;
	}

	public GenerationType getStrategy() {
		return strategy;
	}

	public void setStrategy(GenerationType strategy) {
		this.strategy = strategy;
	}

	public String getGenerator() {
		return generator;
	}

	public void setGenerator(String generator) {
		this.generator = generator;
	}

	public Class<?> getJavaType() {
		return javaType;
	}

	public void setJavaType(Class<?> javaType) {
		this.javaType = javaType;
	}

}
