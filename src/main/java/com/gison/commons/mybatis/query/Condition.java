package com.gison.commons.mybatis.query;

import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

public class Condition {
	private StringBuffer buffer = new StringBuffer();

	private List<Object> params = Lists.newArrayList();

	public StringBuffer getBuffer() {
		return buffer;
	}

	public void setBuffer(StringBuffer buffer) {
		this.buffer = buffer;
	}

	public List<Object> getParams() {
		return params;
	}

	public void setParams(List<Object> params) {
		this.params = params;
	}
	
	//SQL 非空
	public boolean isEmpty(){
		return this.getBuffer().length()==0;
	}
	
	public boolean isMultiple(){
		return this.getBuffer().toString().split("\\?").length > 1;
	}
	
	public static Condition EQ(String field, Object value) {
		Condition condition = new Condition();
		if(value != null){
			condition.getBuffer().append(field).append(" = ?");
			condition.params.add(value);
		}
		return condition;
	}
	
	public static Condition NEQ(String field, Object value) {
		Condition condition = new Condition();
		if(value != null){
			condition.getBuffer().append(field).append(" != ?");
			condition.params.add(value);
		}
		return condition;
	}
	
	public static Condition GT(String field, Object value) {
		Condition condition = new Condition();
		if(value!=null) {
			condition.getBuffer().append(field).append(" > ?");
			condition.params.add(value);
		}
		return condition;
	}
	
	public static Condition NGT(String field, Object value) {
		Condition condition = new Condition();
		if(value!=null) {
			condition.getBuffer().append(field).append(" <= ?");
			condition.params.add(value);
		}
		return condition;
	}
	
	public static Condition LT(String field, Object value) {
		Condition condition = new Condition();
		if(value!=null) {
			condition.getBuffer().append(field).append(" < ?");
			condition.params.add(value);
		}
		return condition;
	}
	
	public static Condition NLT(String field, Object value) {
		Condition condition = new Condition();
		if(value!=null) {
			condition.getBuffer().append(field).append(" >= ?");
			condition.params.add(value);
		}
		return condition;
	}
	
	public static Condition LIKE(String field, Object value) {
		Condition condition = new Condition();
		if(value!=null) {
			condition.getBuffer().append(field).append(" like ?");
			condition.params.add(value);
		}
		return condition;
	}
	
	public static Condition LIKE(String field, Object value, String prev, String next) {
		Condition condition = new Condition();
		if(value!=null) {
			condition.getBuffer().append(field).append(" like ?");
			condition.params.add(StringUtils.defaultString(prev) + value + StringUtils.defaultString(next));
		}
		return condition;
	}
	
	public static Condition IN(String field, Object ...values) {
		Condition condition = new Condition();
		if(values!=null&&values.length>0){
			StringBuffer sql_seats=new StringBuffer("");
			for(Object value:values){
				sql_seats.append(",?");
				condition.params.add(value);
			}
			condition.getBuffer().append(field).append(" in (").append(sql_seats.substring(1)).append(")");
		}
		return condition;
	}

	public static Condition NIN(String field, Object ...values) {
		Condition condition = new Condition();
		if(values!=null&&values.length>0){
			StringBuffer sql_seats=new StringBuffer("");
			for(Object value:values){
				sql_seats.append(",?");
				condition.params.add(value);
			}
			condition.getBuffer().append(field).append(" not in (").append(sql_seats.substring(1)).append(")");
		}
		return condition;
	}
	
	public static Condition BW(String field, Object value1, Object value2) {
		Condition condition = new Condition();
		condition.getBuffer().append(field).append(" between ? and ?");
		condition.params.add(value1);
		condition.params.add(value2);
		return condition;
	}
	
	public static Condition NUL(String field) {
		Condition condition = new Condition();
		condition.getBuffer().append(field).append(" is null");
		return condition;
	}
	
	public static Condition SQL(String sql,Object ...values) {
		Condition condition = new Condition();
		condition.getBuffer().append(sql);
		if(values!=null){
			for(Object value:values){
				condition.params.add(value);
			}
		}
		return condition;
	}
	
	public static Condition SQL(String sql) {
		Condition condition = new Condition();
		condition.getBuffer().append(sql);
		return condition;
	}
	
	public Condition and(Condition condition){
		if(condition.isEmpty()) {
			{
				return this;
			}
		}
		boolean bPrefix = this.isEmpty();
//		boolean bPrevkh = this.isMultiple();
		boolean multiple = condition.isMultiple();
		this.getBuffer().append(bPrefix ? "" : " AND ").append(multiple ? "(" : "").append(condition.getBuffer()).append(multiple ? ")" : "");
		this.params.addAll(condition.getParams());
		return this;
	}
	
	public static Condition AND(Condition ...conditions){
		Condition _condition = new Condition();
		for(Condition condition : conditions){
			if(condition.isEmpty()) {
				continue;
			}
			boolean multiple = condition.isMultiple();
			_condition.getBuffer().append(" AND ").append(multiple ? "(" : "").append(condition.getBuffer()).append(multiple ? ")" : "");
			_condition.params.addAll(condition.params);
		}
		_condition.getBuffer().delete(0, 5);
		return _condition;
	}
	
	public Condition or(Condition condition){
		if(condition.isEmpty()) {
			return this;
		}
		boolean bPrefix = this.isEmpty();
//		boolean bPrevkh = this.isMultiple();
		boolean multiple = condition.isMultiple();
		this.getBuffer().append(bPrefix ? "" : " OR ").append(multiple ? "(" : "").append(condition.getBuffer()).append(multiple ? ")" : "");
		this.params.addAll(condition.getParams());
		return this;
	}
	
	public static Condition OR(Condition ...conditions){
		Condition _condition = new Condition();
		for(Condition condition : conditions){
			if(condition.isEmpty()) {
				continue;
			}
			boolean multiple = condition.isMultiple();
			_condition.getBuffer().append(" OR ").append(multiple ? "(" : "").append(condition.getBuffer()).append(multiple ? ")" : "");
			_condition.params.addAll(condition.params);
		}
		_condition.getBuffer().delete(0, 4);
		return _condition;
	}
	
	public static Condition EMPEY(){
		return new Condition();
	}
	
	public Criterion toCriterion(){
		return new Criterion(this);
	}
	
	public static void main(String[] args) {
		List<Integer> list = new ArrayList<>();
		list.add(1);
		list.add(2);
		Condition condition1 = Condition.IN("username", list.toArray());
		System.out.println(condition1.getBuffer().toString());

	}
}
