package com.gison.commons.mybatis.query;

import java.util.ArrayList;
import java.util.List;


@SuppressWarnings("serial")
public class Pagination<T> extends ArrayList<T> {
	/**总记录数*/
	private long total;
	/**当前页*/
	private int page;
	/**每页记录数*/
	private int size;

	public Pagination() {
	}

	public Pagination(long total, int page, int size, List<T> items) {
		this.total = total;
		this.page = page;
		this.size = size;
		this.addAll(items);
	}

	public long getTotal() {
		return total;
	}

	public void setTotal(long total) {
		this.total = total;
	}

	public List<T> getItems() {
		return this;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}
	/** 每页记录数*/
	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}
	/** 总页数*/
	public int getPages(){
		return (int)Math.ceil(this.total/(this.size*1.0));
	}
}
