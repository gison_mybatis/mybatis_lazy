package com.gison.commons.mybatis.query;

public class Bounds {

	public static final String BOUNDS = "bounds";
	
	private int from;

	private int size;

	public Bounds() {
	}

	public Bounds(int from, int size) {
		this.from = from;
		this.size = size;
	}

	public int getFrom() {
		return from;
	}

	public void setFrom(int from) {
		this.from = from;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

}
