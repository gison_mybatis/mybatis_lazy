package com.gison.commons.mybatis.dialect.impl;


import com.gison.commons.mybatis.dialect.IDialect;

public class Db2Dialect implements IDialect {

	@Override
    public String createPagingSql(String sql, int offset, int size) {
		String _sql_format = "select * from (select _tbl_.*,rownumber() over() as row_num from (%s) as _tbl_) where row_num between %d and %d";
		return String.format(_sql_format, sql, offset + 1, offset + size + 1);
	}

	@Override
    public String createIdentitySql() {
		return "VALUES IDENTITY_VAL_LOCAL()";
	}
}
