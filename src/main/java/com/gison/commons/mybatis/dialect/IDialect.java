package com.gison.commons.mybatis.dialect;



public  interface IDialect {
	 /**
	 * @param sql
	 * @param offset 记录首页位置
	 * @param size 记录数
	 * @return
	 */
	public String createPagingSql(String sql, int offset, int size);

	/**
	 * 查询主键返回
	 * @return
	 */
	public String createIdentitySql();
}
