package com.gison.commons.mybatis.dialect.impl;


import com.gison.commons.mybatis.dialect.IDialect;

public class SqlServer2012Dialect implements IDialect {

	@Override
    public String createPagingSql(String sql, int offset, int size) {
		String _sql_format = "%s OFFSET %d ROWS FETCH NEXT %d ROWS ONLY";
		return String.format(_sql_format, sql, offset, size);
	}

	@Override
    public String createIdentitySql() {
		return "";
	}
}
