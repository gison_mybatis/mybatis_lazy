package com.gison.commons.mybatis.dialect.impl;

import com.gison.commons.mybatis.dialect.IDialect;

public class InformixDialect implements IDialect {

	@Override
    public String createPagingSql(String sql, int offset, int size) {
		String _sql_format = "select skip %d first %d * from (%s) _tbl_";
		return String.format(_sql_format, sql, offset, size);
	}

	@Override
    public String createIdentitySql() {
		return "select dbinfo('sqlca.sqlerrd1') from systables where tabid=1";
	}
}
