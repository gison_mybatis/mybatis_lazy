package com.gison.commons.mybatis.dialect;

public enum Dialect {
    mysql, mariadb, sqlite, oracle, hsqldb, postgresql, sqlserver, db2, informix, h2, sqlserver2012;
    public static Dialect of(String dialect) {
        try {
            return Dialect.valueOf(dialect.toLowerCase());
        } catch (IllegalArgumentException e) {
            String dialects = "";
            for (Dialect d : Dialect.values()) {
            	dialects += "," + d;
            }
            throw new IllegalArgumentException("Mybatis分页插件dialect参数值错误，可选值为[" + dialects.substring(1) + "]");
        }
    }
}
