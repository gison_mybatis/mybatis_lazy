package com.gison.commons.mybatis.dialect.impl;


import com.gison.commons.mybatis.dialect.IDialect;

public class OracleDialect implements IDialect {

	@Override
    public String createPagingSql(String sql, int offset, int size) {
		String _sql_format = "select * from ( select _tbl_.*, rownum row_num from (%s) _tbl_ where rownum <= %d ) where row_num > %d";
		return String.format(_sql_format, sql, offset + size, offset);
	}

	@Override
    public String createIdentitySql() {
		return "";
	}
}
