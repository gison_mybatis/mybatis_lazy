package com.gison.commons.mybatis.dialect.impl;


import com.gison.commons.mybatis.dialect.IDialect;

public class HsqldbDialect implements IDialect {

	@Override
    public String createPagingSql(String sql, int offset, int size) {
		String _sql_format = "%s limit %d offset %d";
		return String.format(_sql_format, sql, size, offset);
	}
	
	@Override
    public String createIdentitySql() {
		return "CALL IDENTITY()";
	}
}
