package com.gison.commons.mybatis.dialect.impl;


import com.gison.commons.mybatis.dialect.IDialect;

public class MysqlDialect implements IDialect {

	@Override
    public String createPagingSql(String sql, int offset, int size) {
		String _sql_format = "%s LIMIT %d,%d";
		return String.format(_sql_format, sql, offset, size);
	}

	@Override
    public String createIdentitySql() {
		return "SELECT LAST_INSERT_ID()";
	}
}
