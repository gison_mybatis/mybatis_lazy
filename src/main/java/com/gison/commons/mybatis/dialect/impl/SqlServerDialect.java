package com.gison.commons.mybatis.dialect.impl;


import com.gison.commons.mybatis.dialect.IDialect;
import com.gison.commons.mybatis.dialect.SqlParser;

public class SqlServerDialect implements IDialect {
	private static final SqlParser sqlParser = new SqlParser();

	@Override
    public String createPagingSql(String sql, int offset, int size) {
		return sqlParser.convertToPageSql(sql, offset, size);
	}

	@Override
    public String createIdentitySql() {
		return "SELECT SCOPE_IDENTITY()";
	}
}
