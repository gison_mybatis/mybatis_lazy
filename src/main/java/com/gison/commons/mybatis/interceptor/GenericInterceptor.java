package com.gison.commons.mybatis.interceptor;

import com.gison.commons.mybatis.dialect.Dialect;
import com.gison.commons.mybatis.dialect.IDialect;
import com.gison.commons.mybatis.dialect.impl.*;
import com.gison.commons.mybatis.query.Parameter;
import com.gison.commons.mybatis.util.MappedStatmentUtil;
import com.gison.commons.mybatis.util.SqlGenerateUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.executor.resultset.ResultSetHandler;
import org.apache.ibatis.executor.statement.StatementHandler;
import org.apache.ibatis.logging.Log;
import org.apache.ibatis.logging.LogFactory;
import org.apache.ibatis.mapping.BoundSql;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.mapping.SqlCommandType;
import org.apache.ibatis.plugin.*;
import org.apache.ibatis.reflection.MetaObject;
import org.apache.ibatis.reflection.SystemMetaObject;

import java.sql.Connection;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

@Intercepts({
	 @Signature(type = StatementHandler.class, method = "prepare", args={Connection.class, Integer.class }),  
	 @Signature(type = ResultSetHandler.class, method = "handleResultSets", args = {Statement.class})
})  
public class GenericInterceptor implements Interceptor {  
  
	private final static String SQL_REGEX = "Generic.entity.*";
	protected static Log log = LogFactory.getLog(GenericInterceptor.class);
	
	@Override
	public Object intercept(Invocation invocation) throws Throwable {
		if (invocation.getTarget() instanceof StatementHandler) {  
			StatementHandler statementHandler = (StatementHandler) invocation.getTarget();  
            MetaObject metaStatementHandler = SystemMetaObject.forObject(statementHandler);  
            MappedStatement mappedStatement=(MappedStatement) metaStatementHandler.getValue("delegate.mappedStatement");
//            MappedStatement mappedStatement = (MappedStatement) invocation.getArgs()[0];
            BoundSql boundSql = (BoundSql) metaStatementHandler.getValue("delegate.boundSql");
            String sourceSql = boundSql.getSql();
            String selectId=mappedStatement.getId();
            Object parameterObject = boundSql.getParameterObject();
            List<Parameter> parameters = new ArrayList<Parameter>();
            if (sourceSql.matches(SQL_REGEX)) {
            	String excuseSQL = null;
            	Class<?> entityClass = MappedStatmentUtil.transResourceToEntityClass(selectId);
            	if(mappedStatement.getSqlCommandType() == SqlCommandType.SELECT){
            		switch (sourceSql.trim()) {
    				case "Generic.entity.select.all":
    					excuseSQL = SqlGenerateUtil.createSqlSelectAll(entityClass);
    					break;
    				case "Generic.entity.select.fields.all":
    					excuseSQL = SqlGenerateUtil.createSqlSelectFieldsAll(entityClass, parameterObject);
    					break;
    				case "Generic.entity.select.single.id":
    					excuseSQL = "select * from user where id = ?;";
    					break;
    				default:
    					break;
    				}
            	}
            	
//            	MapperMethod.ParamMap<Object> paramMap =  new MapperMethod.ParamMap<Object>();
//    			paramMap.put("id", 3);
//    			metaStatementHandler.setValue("delegate.boundSql.parameterObject", paramMap);
//    			ParameterMapping  mapping = new ParameterMapping.Builder(mappedStatement.getConfiguration(), "id", Integer.class).build();
//    			List<ParameterMapping> list =  boundSql.getParameterMappings();
//    			list.add(mapping);
//            	invocation.getArgs()[0] = MappedStatmentUtil.createGenericMappedStatement(entityClass,mappedStatement, excuseSQL, parameterObject, parameters, dialect);
//            	metaStatementHandler.setValue("delegate.boundSql.sql", excuseSQL);
//            	ParameterHandler 
            	
//        		Configuration configuration = mappedStatement.getConfiguration();
//        		Connection connection = (Connection) invocation.getArgs()[0];  
//        		PreparedStatement ps = null;
//        		ps = connection.prepareStatement(excuseSQL);
//        		
//        		ps.setInt(0, 1);
//        		mappedStatement.getConfiguration().newParameterHandler(mappedStatement, parameterObject, boundSql).setParameters(ps);;
//        		invocation.
//        		mappedStatement.
//        		metaStatementHandler.
//        		List<ParameterMapping> parameterMappings = Lists.newArrayList();
//        		StaticSqlSource staticSqlSource = new StaticSqlSource(configuration, sql, parameterMappings);
//            	MappedStatement.Builder builder = new MappedStatement.Builder(configuration, id, sqlSource, sqlCommandType)
            }

		}
		return invocation.proceed();
		
	}
		
	
	/**
     * 拦截类型StatementHandler 
     */
	@Override
	public Object plugin(Object target) {
		if (target instanceof StatementHandler) {  
            return Plugin.wrap(target, this);  
        } else {  
            return target;  
        }  
	}
	
	@Override
	public void setProperties(Properties properties) {
		String _dialectType = properties.getProperty("dialect");
		if (StringUtils.isEmpty(_dialectType)) {
			dialect = new MysqlDialect();
			return;
		}
		Dialect _dialect = Dialect.of(_dialectType);
		switch (_dialect) {
		case mysql:
		case mariadb:
		case sqlite:
			dialect = new MysqlDialect();
			break;
		case oracle:
			dialect = new OracleDialect();
			break;
		case hsqldb:
			dialect = new HsqldbDialect();
			break;
		case sqlserver:
			dialect = new SqlServerDialect();
			break;
		case sqlserver2012:
			dialect = new SqlServer2012Dialect();
			break;
		case db2:
			dialect = new Db2Dialect();
			break;
		case postgresql:
			dialect = new PostgreSqlDialect();
			break;
		case informix:
			dialect = new InformixDialect();
			break;
		case h2:
			dialect = new H2Dialect();
			break;
		default:
			dialect = new MysqlDialect();
		}
	}  	
	
	public void setPageCount(){
		
	}
	
	private IDialect dialect;
	
}  