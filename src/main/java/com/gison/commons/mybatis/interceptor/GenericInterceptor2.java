package com.gison.commons.mybatis.interceptor;

import com.gison.commons.mybatis.dialect.Dialect;
import com.gison.commons.mybatis.dialect.IDialect;
import com.gison.commons.mybatis.dialect.impl.*;
import com.gison.commons.mybatis.query.Parameter;
import com.gison.commons.mybatis.util.MappedStatmentUtil;
import com.gison.commons.mybatis.util.SqlGenerateUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.executor.CachingExecutor;
import org.apache.ibatis.executor.Executor;
import org.apache.ibatis.executor.SimpleExecutor;
import org.apache.ibatis.logging.Log;
import org.apache.ibatis.logging.LogFactory;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.mapping.SqlCommandType;
import org.apache.ibatis.plugin.*;
import org.apache.ibatis.reflection.DefaultReflectorFactory;
import org.apache.ibatis.reflection.MetaObject;
import org.apache.ibatis.reflection.factory.DefaultObjectFactory;
import org.apache.ibatis.reflection.wrapper.DefaultObjectWrapperFactory;
import org.apache.ibatis.session.ResultHandler;
import org.apache.ibatis.session.RowBounds;

import java.util.List;
import java.util.Properties;

@Intercepts({
	@Signature(type = Executor.class, method = "query", args = { MappedStatement.class, Object.class, RowBounds.class, ResultHandler.class }),
	@Signature(type = Executor.class, method = "update", args = { MappedStatement.class, Object.class }) })
public class GenericInterceptor2 implements Interceptor {  
  
	private final static String SQL_REGEX = "Generic.entity.*";
	protected static Log log = LogFactory.getLog(GenericInterceptor2.class);

    @Override
	public Object intercept(Invocation invocation) throws Throwable {
		Object targetObject = invocation.getTarget();
		MetaObject executorMetaObject = MetaObject.forObject(targetObject, new DefaultObjectFactory(),
				new DefaultObjectWrapperFactory(), new DefaultReflectorFactory());
		while (executorMetaObject.hasGetter("h") || executorMetaObject.hasGetter("target")) {
			Object _hTargetObj = executorMetaObject.hasGetter("h") ? executorMetaObject.getValue("h") : null;
			Object _tTargetObj = executorMetaObject.hasGetter("target") ? executorMetaObject.getValue("target") : null;
			targetObject = _hTargetObj != null ? _hTargetObj : _tTargetObj;
			executorMetaObject = MetaObject.forObject(targetObject, new DefaultObjectFactory(),
					new DefaultObjectWrapperFactory(), new DefaultReflectorFactory());
		}
		if (targetObject instanceof CachingExecutor|| targetObject instanceof SimpleExecutor) {
			final MappedStatement mappedStatement = (MappedStatement) invocation.getArgs()[0];
			final Object parameterObject = invocation.getArgs()[1];
			final String sourceSql = mappedStatement.getBoundSql(parameterObject).getSql();
            if (sourceSql.matches(SQL_REGEX)) {
            	String excuseSQL = null;
            	List<Parameter> parameters = null;
            	Class<?> entityClass = MappedStatmentUtil.transResourceToEntityClass(mappedStatement.getId());
            	if(mappedStatement.getSqlCommandType() == SqlCommandType.SELECT){
            		switch (sourceSql.trim()) {
    				case "Generic.entity.select.all":
    					excuseSQL = SqlGenerateUtil.createSqlSelectAll(entityClass);
    					parameters = SqlGenerateUtil.createParamsSelectAll();
    					break;
    				case "Generic.entity.select.fields.all":
    					excuseSQL = SqlGenerateUtil.createSqlSelectFieldsAll(entityClass, parameterObject);
    					parameters = SqlGenerateUtil.createParamsSelectAll();
    					break;
    				case "Generic.entity.select.all.count":
    					excuseSQL = SqlGenerateUtil.createSqlSelectAllCount(entityClass);
    					parameters = SqlGenerateUtil.createParamsSelectAllCount();
    					break;
    				case "Generic.entity.select.id":
    					excuseSQL = SqlGenerateUtil.createSqlSelectId(entityClass);
    					parameters = SqlGenerateUtil.createParamsSelectId(entityClass, parameterObject);
    					break;
    				case "Generic.entity.select.fields.id":
    					excuseSQL = SqlGenerateUtil.createSqlSelectFieldsId(entityClass, parameterObject);
    					parameters = SqlGenerateUtil.createParamsSelectFieldsId(entityClass, parameterObject);
    					break;	
    				case "Generic.entity.select.ids":
    					excuseSQL = SqlGenerateUtil.createSqlSelectIds(entityClass, parameterObject, false);
    					parameters = SqlGenerateUtil.createParamsSelectIds(entityClass, parameterObject);
    					break;	
    				case "Generic.entity.select.fields.ids":
    					excuseSQL = SqlGenerateUtil.createSqlSelectIds(entityClass, parameterObject, true);
    					parameters = SqlGenerateUtil.createParamsSelectIds(entityClass, parameterObject);
    					break;
    				case "Generic.entity.select.condition.unique":
    					excuseSQL = SqlGenerateUtil.createSqlSelectCondition(entityClass, parameterObject);
						parameters = SqlGenerateUtil.createParamsSelectCondition(entityClass,parameterObject);
    					break;
    				case "Generic.entity.select.condition.multiple":
    					excuseSQL = SqlGenerateUtil.createSqlSelectCondition(entityClass, parameterObject);
						parameters = SqlGenerateUtil.createParamsSelectCondition(entityClass,parameterObject);
    					break;
					case "Generic.entity.select.condition.count":
						excuseSQL = SqlGenerateUtil.createSqlSelectConditionCount(entityClass, parameterObject);
						parameters = SqlGenerateUtil.createParamsSelectConditionCount(entityClass,parameterObject);
						break;
    				case "Generic.entity.select.sql.single":
    					excuseSQL = SqlGenerateUtil.createSqlExcute(entityClass, parameterObject);
						if(!excuseSQL.trim().toLowerCase().startsWith("select")){throw new RuntimeException("SQL NOT SELECT sql:"+excuseSQL);}
						parameters = SqlGenerateUtil.createParamsExcute(entityClass,parameterObject);
    					break;
    				case "Generic.entity.select.sql.number":
    					excuseSQL = SqlGenerateUtil.createSqlExcute(entityClass, parameterObject);
						if(!excuseSQL.trim().toLowerCase().startsWith("select")){throw new RuntimeException("SQL NOT SELECT sql:"+excuseSQL);}
						parameters = SqlGenerateUtil.createParamsExcute(entityClass,parameterObject);
    					break;
    				case "Generic.entity.select.sql.string":
    					excuseSQL = SqlGenerateUtil.createSqlExcute(entityClass, parameterObject);
						if(!excuseSQL.trim().toLowerCase().startsWith("select")){throw new RuntimeException("SQL NOT SELECT sql:"+excuseSQL);}
						parameters = SqlGenerateUtil.createParamsExcute(entityClass,parameterObject);
    					break;
    				case "Generic.entity.select.sql.multiple":
    					excuseSQL = SqlGenerateUtil.createSqlExcute(entityClass, parameterObject);
						if(!excuseSQL.trim().toLowerCase().startsWith("select")){throw new RuntimeException("SQL NOT SELECT sql:"+excuseSQL);}
						parameters = SqlGenerateUtil.createParamsExcute(entityClass,parameterObject);
    					break;
    				case "Generic.entity.select.sql.count":
    					excuseSQL = SqlGenerateUtil.createSqlExcute(entityClass, parameterObject);
						if(!excuseSQL.trim().toLowerCase().startsWith("select")){throw new RuntimeException("SQL NOT SELECT sql:"+excuseSQL);}
						parameters = SqlGenerateUtil.createParamsExcute(entityClass,parameterObject);
    					break;
    				case "Generic.entity.selects.condition.paging":
    					excuseSQL = SqlGenerateUtil.createSqlSelectConditionPaging(entityClass, parameterObject);
						parameters = SqlGenerateUtil.createParamsSelectConditionPaging(entityClass,parameterObject);
    					break;
    				default:
    					break;
    				}
            	}else if (mappedStatement.getSqlCommandType() == SqlCommandType.INSERT) {
            		switch (sourceSql.trim()) {
    				case "Generic.entity.insert.single":
    					excuseSQL = SqlGenerateUtil.createSqlInsertSingle(entityClass);
						parameters = SqlGenerateUtil.createParamsInsertSingle(entityClass, parameterObject,true);
    					break;
	            	case "Generic.entity.replace.single":
	            		excuseSQL = SqlGenerateUtil.createSqlReplaceSingle(entityClass);
						parameters = SqlGenerateUtil.createParamsRepleceSingle(entityClass, parameterObject);
						break;
	            	case "Generic.entity.insert.multiple":
	            		excuseSQL = SqlGenerateUtil.createSqlInsertMultiple(entityClass,parameterObject);
						parameters = SqlGenerateUtil.createParamsInsertMultiple(entityClass, parameterObject);
						break;
	            	case "Generic.entity.insert.sql":
	            		excuseSQL = SqlGenerateUtil.createSqlExcute(entityClass, parameterObject);
						if(!excuseSQL.trim().toLowerCase().startsWith("insert")){throw new RuntimeException("SQL NOT INSERT sql:"+excuseSQL);}
						parameters = SqlGenerateUtil.createParamsExcute(entityClass,parameterObject);
						break;
					default:break;
	        		}
            	}else if (mappedStatement.getSqlCommandType() == SqlCommandType.UPDATE) {
            		switch (sourceSql.trim()) {
    				case "Generic.entity.update.single":
    					excuseSQL = SqlGenerateUtil.createSqlUpdateSingle(entityClass);
						parameters = SqlGenerateUtil.createParamsUpdateSingle(entityClass, parameterObject);
    					break;
    				case "Generic.entity.update.notnul":
    					excuseSQL = SqlGenerateUtil.createSqlUpdateNotNul(entityClass,parameterObject);
						parameters = SqlGenerateUtil.createParamsUpdateNotNul(entityClass, parameterObject);
    					break;
    				case "Generic.entity.update.single.criterion":
    					excuseSQL = SqlGenerateUtil.createSqlUpdateCondition(entityClass, parameterObject);
						parameters = SqlGenerateUtil.createParamsUpdateCondition(entityClass,parameterObject);
    					break;
    				case "Generic.entity.update.notnul.criterion":
    					excuseSQL = SqlGenerateUtil.createSqlUpdateNotNulCondition(entityClass, parameterObject);
						parameters = SqlGenerateUtil.createParamsUpdateNotNulCondition(entityClass,parameterObject);
    					break;
    				case "Generic.entity.update.sql":
    					excuseSQL = SqlGenerateUtil.createSqlExcute(entityClass, parameterObject);
						if(!excuseSQL.trim().toLowerCase().startsWith("update")){throw new RuntimeException("SQL NOT UPDATE sql:"+excuseSQL);}
						parameters = SqlGenerateUtil.createParamsExcute(entityClass,parameterObject);
    					break;
					default:break;
            		}
            	}else if (mappedStatement.getSqlCommandType() == SqlCommandType.DELETE) {
            		switch (sourceSql.trim()) {
    				case "Generic.entity.delete.id":
    					excuseSQL = SqlGenerateUtil.createSqlDeleteId(entityClass);
						parameters = SqlGenerateUtil.createParamsDeleteId(entityClass, parameterObject);
    					break;
    				case "Generic.entity.delete.ids":
    					excuseSQL = SqlGenerateUtil.createSqlDeleteIds(entityClass,parameterObject);
						parameters = SqlGenerateUtil.createParamsDeleteIds(entityClass, parameterObject);
    					break;
    				case "Generic.entity.delete.sql":
    					excuseSQL = SqlGenerateUtil.createSqlExcute(entityClass, parameterObject);
						if(!excuseSQL.trim().toLowerCase().startsWith("delete")){throw new RuntimeException("SQL NOT DELETE sql:"+excuseSQL);}
						parameters = SqlGenerateUtil.createParamsExcute(entityClass,parameterObject);
    					break;
    				case "Generic.entity.delete.condition":
    					excuseSQL = SqlGenerateUtil.createSqlDeleteCondition(entityClass, parameterObject);
						parameters = SqlGenerateUtil.createParamsDeleteCondition(entityClass,parameterObject);
    					break;
					default:break;
            		}

            	}
            	
            	invocation.getArgs()[0] = MappedStatmentUtil.createGenericMappedStatement(entityClass,mappedStatement, excuseSQL, parameterObject, parameters, dialect);

            }

		}
		return invocation.proceed();
		
	}
		
	
	/**
     * 拦截类型StatementHandler 
     */
    @Override
	public Object plugin(Object target) {
		return Plugin.wrap(target, this); 
//		if (target instanceof StatementHandler) {  
//            return Plugin.wrap(target, this);  
//        } else {  
//            return target;  
//        }  
	}

    @Override
	public void setProperties(Properties properties) {
		String _dialectType = properties.getProperty("dialect");
		if (StringUtils.isEmpty(_dialectType)) {
			dialect = new MysqlDialect();
			return;
		}
		Dialect _dialect = Dialect.of(_dialectType);
		switch (_dialect) {
		case mysql:
		case mariadb:
		case sqlite:
			dialect = new MysqlDialect();
			break;
		case oracle:
			dialect = new OracleDialect();
			break;
		case hsqldb:
			dialect = new HsqldbDialect();
			break;
		case sqlserver:
			dialect = new SqlServerDialect();
			break;
		case sqlserver2012:
			dialect = new SqlServer2012Dialect();
			break;
		case db2:
			dialect = new Db2Dialect();
			break;
		case postgresql:
			dialect = new PostgreSqlDialect();
			break;
		case informix:
			dialect = new InformixDialect();
			break;
		case h2:
			dialect = new H2Dialect();
			break;
		default:
			dialect = new MysqlDialect();
		}
	}  	
	
	public void setPageCount(){
		
	}
	
	private IDialect dialect;
	
}  