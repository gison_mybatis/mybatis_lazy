package com.gison.commons.mybatis.interceptor;


import com.gison.commons.mybatis.dialect.Dialect;
import com.gison.commons.mybatis.dialect.IDialect;
import com.gison.commons.mybatis.dialect.impl.*;
import com.gison.commons.mybatis.query.Bounds;
import com.gison.commons.mybatis.query.Pagination;
import com.gison.commons.mybatis.query.Paging;
import com.gison.commons.mybatis.util.MappedStatmentUtil;
import com.gison.commons.mybatis.util.SqlExecutorUtil;
import com.gison.commons.mybatis.util.SqlGenerateUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.binding.MapperMethod;
import org.apache.ibatis.executor.CachingExecutor;
import org.apache.ibatis.executor.Executor;
import org.apache.ibatis.executor.SimpleExecutor;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.mapping.ParameterMapping;
import org.apache.ibatis.mapping.SqlCommandType;
import org.apache.ibatis.plugin.*;
import org.apache.ibatis.reflection.DefaultReflectorFactory;
import org.apache.ibatis.reflection.MetaObject;
import org.apache.ibatis.reflection.factory.DefaultObjectFactory;
import org.apache.ibatis.reflection.wrapper.DefaultObjectWrapperFactory;
import org.apache.ibatis.session.ResultHandler;
import org.apache.ibatis.session.RowBounds;
import org.apache.ibatis.transaction.Transaction;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;


@Intercepts({ @Signature(type = Executor.class, method = "query", args = { MappedStatement.class, Object.class,
		RowBounds.class, ResultHandler.class }) })
public class PaginationInterceptor implements Interceptor {

	@Override
    @SuppressWarnings({ "rawtypes", "unchecked" })
	public Object intercept(Invocation invocation) throws Throwable {
		Object targetObject = invocation.getTarget();
		MetaObject executorMetaObject = MetaObject.forObject(targetObject, new DefaultObjectFactory(),
				new DefaultObjectWrapperFactory(), new DefaultReflectorFactory());
		while (executorMetaObject.hasGetter("h") || executorMetaObject.hasGetter("target")) {
			Object _hTargetObj = executorMetaObject.hasGetter("h") ? executorMetaObject.getValue("h") : null;
			Object _tTargetObj = executorMetaObject.hasGetter("target") ? executorMetaObject.getValue("target") : null;
			targetObject = _hTargetObj != null ? _hTargetObj : _tTargetObj;
			executorMetaObject = MetaObject.forObject(targetObject, new DefaultObjectFactory(),
					new DefaultObjectWrapperFactory(), new DefaultReflectorFactory());
		}

		if (targetObject instanceof CachingExecutor|| targetObject instanceof SimpleExecutor) {
			final MappedStatement mappedStatement = (MappedStatement) invocation.getArgs()[0];
			final Object parameterObject = invocation.getArgs()[1];
			String sourceSql = mappedStatement.getBoundSql(parameterObject).getSql();
			RowBounds rowBounds = (RowBounds) invocation.getArgs()[2];
			if (SqlCommandType.SELECT == mappedStatement.getSqlCommandType()) {
				Bounds bounds=null;
				Paging<?> paging = null;
				if ((parameterObject instanceof Paging)) {
					paging = (Paging<?>) parameterObject;
					rowBounds = new RowBounds(paging.getFrom(), paging.getSize());
				} else if(parameterObject instanceof Bounds){
					bounds = (Bounds) parameterObject;
					rowBounds = new RowBounds(bounds.getFrom(), bounds.getSize());
				} else if (parameterObject instanceof MapperMethod.ParamMap) {
					MapperMethod.ParamMap<?> parameterMap = (MapperMethod.ParamMap<?>) parameterObject;
					if (parameterMap.containsKey(Paging.PAGING)) {
						paging = (Paging<?>) parameterMap.get(Paging.PAGING);
						rowBounds = new RowBounds(paging.getFrom(), paging.getSize());
					} else if(parameterMap.containsKey(Bounds.BOUNDS)) {
						bounds = (Bounds) parameterMap.get(Bounds.BOUNDS);
						rowBounds = new RowBounds(bounds.getFrom(), bounds.getSize());
					} else {
						Iterator<?> itor = parameterMap.values().iterator();
						while (itor.hasNext()) {
							Object obj = itor.next();
							if (obj instanceof Paging) {
								paging = (Paging<?>) obj;
								rowBounds = new RowBounds(paging.getFrom(), paging.getSize());
								break;
							}
							
							if (obj instanceof Bounds) {
								bounds = (Bounds) obj;
								rowBounds = new RowBounds(bounds.getFrom(), bounds.getSize());
								break;
							}
						}
					}
				}
				if (bounds !=null){
					List<ParameterMapping> parameterMappings = mappedStatement.getBoundSql(parameterObject)
							.getParameterMappings();
					invocation.getArgs()[0] = MappedStatmentUtil.createPaginationMappedStatement(mappedStatement,
							sourceSql, parameterObject, parameterMappings, rowBounds, dialect);
					return invocation.proceed();
				}
				if (paging != null) {
					Executor executor = (Executor) invocation.getTarget();
					Transaction transaction = executor.getTransaction();
					List<ParameterMapping> parameterMappings = mappedStatement.getBoundSql(parameterObject)
							.getParameterMappings();
					long iTotal = SqlExecutorUtil.getCount(transaction.getConnection(), mappedStatement,
							parameterObject, SqlGenerateUtil.createSqlSelectCount(sourceSql), parameterMappings);
					invocation.getArgs()[0] = MappedStatmentUtil.createPaginationMappedStatement(mappedStatement,
							sourceSql, parameterObject, parameterMappings, rowBounds, dialect);
					List<?> items=iTotal > 0 ? ((List) invocation.proceed()) : Collections.EMPTY_LIST;
					return new Pagination(iTotal, paging.getPage(), paging.getSize(), items);
				}
			}
		}
		return invocation.proceed();
	}

	@Override
    public Object plugin(Object object) {
		return Plugin.wrap(object, this);
	}

	@Override
    public void setProperties(Properties properties) {
		String _dialectType = properties.getProperty("dialect");
		if (StringUtils.isEmpty(_dialectType)) {
			dialect = new MysqlDialect();
			return;
		}
		Dialect _dialect = Dialect.of(_dialectType);
		switch (_dialect) {
		case mysql:
		case mariadb:
		case sqlite:
			dialect = new MysqlDialect();
			break;
		case oracle:
			dialect = new OracleDialect();
			break;
		case hsqldb:
			dialect = new HsqldbDialect();
			break;
		case sqlserver:
			dialect = new SqlServerDialect();
			break;
		case sqlserver2012:
			dialect = new SqlServer2012Dialect();
			break;
		case db2:
			dialect = new Db2Dialect();
			break;
		case postgresql:
			dialect = new PostgreSqlDialect();
			break;
		case informix:
			dialect = new InformixDialect();
			break;
		case h2:
			dialect = new H2Dialect();
			break;
		default:
			dialect = new MysqlDialect();
		}
	}

	private IDialect dialect;
}
