package com.gison.commons.mybatis.enmu;

public enum GenerationType {
	/**IDENTITY, TABLE, SEQUENCE, AUTO 暂未实现*/
	JDBC, UUID, IDENTITY, TABLE, SEQUENCE, AUTO ;

	public static GenerationType of(String name) {
		return GenerationType.valueOf(name);
	}

}

