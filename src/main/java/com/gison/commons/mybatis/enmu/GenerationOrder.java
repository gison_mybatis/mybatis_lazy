package com.gison.commons.mybatis.enmu;

public enum GenerationOrder {
	BEFORE, AFTER;

	public static GenerationOrder of(String name) {
		return GenerationOrder.valueOf(name);
	}

}

