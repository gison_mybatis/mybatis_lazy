package com.gison.commons.mybatis.util;

import com.gison.commons.mybatis.annotation.*;
import com.gison.commons.mybatis.enmu.GenerationOrder;
import com.gison.commons.mybatis.enmu.GenerationType;
import com.gison.commons.mybatis.query.Criterion;
import com.gison.commons.mybatis.query.Parameter;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.binding.MapperMethod;
import org.apache.ibatis.session.defaults.DefaultSqlSession;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class SqlGenerateUtil {
	public static final Class<? extends Annotation> JPA_ID = Id.class;
	public static final Class<? extends Annotation> JPA_TABLE = Table.class;
	public static final Class<? extends Annotation> JPA_COLUMN = Column.class;
	public static final Class<? extends Annotation> JPA_TRANSIENT = Transient.class;
	public static final Class<? extends Annotation> JPA_GENERATED_VALUE = GeneratedValue.class;
	public static Map<String, String> sql_cache = Maps.newConcurrentMap();
	
	/*----------------------------------------------创建增删改sql-start------------------------------------------------------------------*/
	public static String createSqlInsertSingle(Class<?> clazz) {
		String key_insert=clazz.getName()+".createSqlInsertSingle";
		String sql_insert=sql_cache.get(key_insert);
		if(sql_insert==null){
			String _sql_format = "INSERT INTO %s (%s) VALUES (%s)";
			String _table_name = sql_dbtable_name(clazz);
			String _filed_insert = sql_insert_filed(clazz);
			String _value_insert = sql_insert_value(clazz);
			sql_insert=String.format(_sql_format, _table_name, _filed_insert, _value_insert);
			sql_cache.put(key_insert, sql_insert);
		}
		return sql_insert;
	}
	
	public static String createSqlReplaceSingle(Class<?> clazz) {
		String key_insert=clazz.getName()+".createSqlReplaceSingle";
		String sql_insert=sql_cache.get(key_insert);
		if(sql_insert==null){
			String _sql_format = "REPLACE INTO %s (%s) VALUES (%s)";
			String _table_name = sql_dbtable_name(clazz);
			String _filed_insert = sql_insert_filed(clazz);
			String _value_insert = sql_insert_value(clazz);
			sql_insert= String.format(_sql_format, _table_name, _filed_insert, _value_insert);
			sql_cache.put(key_insert, sql_insert);
		}
		return sql_insert;
	}
	public static String createSqlInsertMultiple(Class<?> clazz, Object listObject) {
		String _sql_format = "INSERT INTO %s (%s) VALUES (%s)";
		String _table_name = sql_dbtable_name(clazz);
		String _filed_insert = sql_insert_filed(clazz);
		String _value_insert = sql_insert_value(clazz);
		String _sql_insert = String.format(_sql_format, _table_name, _filed_insert, _value_insert);
		if (listObject != null) {
			List<?> list = (List<?>) ((Map<?, ?>) listObject).get("list");
			if (list != null && list.size() > 1) {
				for (int i = 0; i < list.size() - 1; i++) {
					_sql_insert += ",(" + _value_insert + ")";
				}
			}
		}

		return _sql_insert;
	}
	
	public static String createSqlUpdateSingle(Class<?> clazz) {
		String key_update=clazz.getName()+".createSqlUpdateSingle";
		String sql_update=sql_cache.get(key_update);
		if(sql_update==null){
			String _sql_format = "UPDATE %s SET %s WHERE %s";
			String _table_name = sql_dbtable_name(clazz);
			String _filed_update = sql_update_filed(clazz);
			String _where_select = sql_select_where_id(clazz);
			sql_update= String.format(_sql_format, _table_name, _filed_update, _where_select);
			sql_cache.put(key_update, sql_update);
		}
		return sql_update;
		
	}
	
	public static String createSqlUpdateNotNul(Class<?> clazz, Object clazzObject) {
		String _sql_format = "UPDATE %s SET %s WHERE %s";
		String _table_name = sql_dbtable_name(clazz);
		String _filed_update = sql_update_filed(clazz, clazzObject);
		String _where_select = sql_select_where_id(clazz);
		return String.format(_sql_format, _table_name, _filed_update, _where_select);
	}
	
	public static String createSqlUpdateCondition(Class<?> clazz, Object mapObject) {
		String _sql_format = "UPDATE %s SET %s %s";
		String _table_name = sql_dbtable_name(clazz);
		String _filed_update = sql_update_filed(clazz);
		String _where_select = "";
		if (mapObject != null) {
			if (mapObject instanceof Map) {
				MapperMethod.ParamMap<?> parameterMap = (MapperMethod.ParamMap<?>) mapObject;
				Criterion _criterion = (Criterion) parameterMap.get("param2");
				_where_select = _criterion==null?"":_criterion.toSQL();
			}
		}
		return String.format(_sql_format, _table_name, _filed_update, _where_select);
	}
	
	public static String createSqlUpdateNotNulCondition(Class<?> clazz, Object mapObject) {
		String _sql_format = "UPDATE %s SET %s %s";
		String _table_name = sql_dbtable_name(clazz);
		String _filed_update = "";
		String _where_select = "";
		if (mapObject != null) {
			if (mapObject instanceof Map) {
				MapperMethod.ParamMap<?> parameterMap = (MapperMethod.ParamMap<?>) mapObject;
				Criterion _criterion = (Criterion) parameterMap.get("param2");
				Object _clazzObj = parameterMap.get("param1");
				_filed_update=sql_update_filed(clazz,_clazzObj);
				_where_select = _criterion==null?"":_criterion.toSQL();
			}
		}
		return String.format(_sql_format, _table_name, _filed_update, _where_select);
	}
	
	public static String createSqlDeleteId(Class<?> clazz) {
		String key_delete=clazz.getName()+".createSqlDeleteId";
		String sql_delete=sql_cache.get(key_delete);
		if(sql_delete==null){
			String _sql_format = "DELETE FROM %s WHERE %s";
			String _table_name = sql_dbtable_name(clazz);
			String _where_select = sql_select_where_id(clazz);
			sql_delete= String.format(_sql_format, _table_name, _where_select);
			sql_cache.put(key_delete, sql_delete);
		}
		return sql_delete;
		
	}
	
	public static String createSqlDeleteIds(Class<?> clazz, Object map) {
		String _sql_format = "DELETE FROM %s WHERE %s";
		String _table_name = sql_dbtable_name(clazz);
		String _where_delete = "";
		if (map != null) {
			List<?> list = (List<?>) ((Map<?, ?>) map).get("list");
			if (list != null && list.size() > 0) {
				String where_select = sql_select_where_id(clazz);
				for (int i = 0; i < list.size(); i++) {
					_where_delete += " or (" + where_select + ")";
				}
				_where_delete = _where_delete.substring(4);
			}
		}
		return String.format(_sql_format, _table_name, _where_delete);
	}
	
	public static String createSqlDeleteCondition(Class<?> clazz, Object criterion) {
		String _sql_format = "DELETE FROM %s %s";
		String _table_name = sql_dbtable_name(clazz);
		String _where_select = "";
		if (criterion != null) {
			if (criterion instanceof Criterion) {
				_where_select = ((Criterion) criterion).toSQL();
			}
		}
		return String.format(_sql_format, _table_name, _where_select);
	}
	/*----------------------------------------------创建插入sql-end------------------------------------------------------------------*/
	
	/*----------------------------------------------创建查询sql-start------------------------------------------------------------------*/
	/**
	 * 创建根据id查询的sql
	 * @param clazz
	 * @return
	 */
	public static String createSqlSelectId(Class<?> clazz) {
		String key_select=clazz.getName()+".createSqlSelectId";
		String sql_select=sql_cache.get(key_select);
		if(sql_select==null){
			String _sql_format = "SELECT %s FROM %s WHERE %s";
			String _table_name = sql_dbtable_name(clazz);
			String _filed_select = sql_select_filed(clazz);
			String _where_select = sql_select_where_id(clazz);
			sql_select= String.format(_sql_format, _filed_select, _table_name, _where_select);
			sql_cache.put(key_select, sql_select);
		}
		return sql_select;	
	}
	
	/**
	 * 创建根据id查询指定字段的sql
	 * @param clazz
	 * @param fields
	 * @return
	 */
	public static String createSqlSelectFieldsId(Class<?> clazz, Object fields) {
		if(fields == null || !(fields instanceof Map)){
			return createSqlSelectId(clazz);
		}
		String _sql_format = "SELECT %s FROM %s WHERE %s";
		String _table_name = sql_dbtable_name(clazz);
		MapperMethod.ParamMap<?> fieldMap = (MapperMethod.ParamMap<?>)fields;
		String _filed_select = sql_select_filed(clazz, fieldMap.get("fileds"));
		String _where_select = sql_select_where_id(clazz);
		String sql_select= String.format(_sql_format, _filed_select, _table_name, _where_select);
		return sql_select;	
	}
	
	/**创建查询所有数据的sql*/
	public static String createSqlSelectAll(Class<?> clazz) {
		String key_select = clazz.getName()+".createSqlSelectAll";
		String sql_select = sql_cache.get(key_select);
		if(sql_select == null){
			String sql_format = "select %s from %s";
			String table_name = sql_dbtable_name(clazz);
			String _filed_select = sql_select_filed(clazz);
			sql_select= String.format(sql_format, _filed_select, table_name);
			sql_cache.put(key_select, sql_select);
		}
		return sql_select;
	}
	
	/**创建查询所有数据指定字段的sql*/
	public static String createSqlSelectFieldsAll(Class<?> clazz, Object fields) {
		if(fields == null || !(fields instanceof Map)){
			return createSqlSelectAll(clazz);
		}
		String sql_format = "select %s from %s";
		String table_name = sql_dbtable_name(clazz);
		DefaultSqlSession.StrictMap<?> fieldMap = (DefaultSqlSession.StrictMap<?>)fields;
		String _filed_select = sql_select_filed(clazz, fieldMap.get("array"));
		return String.format(sql_format, _filed_select, table_name);
	}
	
	public static String createSqlSelectAllCount(Class<?> clazz){
		String key_select = clazz.getName()+".createSqlSelectAllCount";
		String sql_select = sql_cache.get(key_select);
		if(sql_select == null){
			String sql_format = "select count(*) from %s";
			String table_name = sql_dbtable_name(clazz);
			sql_select= String.format(sql_format, table_name);
			sql_cache.put(key_select, sql_select);
		}
		return sql_select;
	}
	
	public static String createSqlSelectIds(Class<?> clazz, Object map, boolean isFieldsQuery) {
		String _sql_format = "SELECT %s FROM %s WHERE %s";
		String _table_name = sql_dbtable_name(clazz);
		String _field_select = "";
		if(isFieldsQuery){
			_field_select = sql_select_filed(clazz, ((Map<?, ?>) map).get("fields"));
		}else{
			_field_select = sql_select_filed(clazz);
		}
		String _where_select = "";
		if (map != null) {
			List<?> list = (List<?>) ((Map<?, ?>) map).get("list");
			if (list != null && list.size() > 0) {
				StringBuilder idBulider = new StringBuilder("");
				Field[] fields = getFields(clazz);
				for (Field field : fields) {
					if (field.isAnnotationPresent(JPA_ID)) {
						if (field.isAnnotationPresent(JPA_COLUMN)) {
							Column column = (Column) field.getAnnotation(JPA_COLUMN);
							String _escape = column.escape() ? "`" : "";
							String _column = StringUtils.isEmpty(column.name()) ? field.getName() : column.name();
							idBulider.append(_escape).append(_column).append(_escape);
						}else {
							idBulider.append(field.getName());
						}
						break;
					}
				}
				_where_select = idBulider.append(" in(").append(
						StringUtils.repeat("?,", list.size())).toString();
				_where_select = _where_select.substring(0, _where_select.length()-1) + ")";
			}
		}
		return String.format(_sql_format, _field_select, _table_name, _where_select);
	}
	
	public static String createSqlSelectCondition(Class<?> clazz, Object param) {
		String _sql_format = "SELECT %s FROM %s %s";
		String _filed_select = "";
		String _table_name = sql_dbtable_name(clazz);
		String _where_select = "";
		if (param != null) {
			if (param instanceof Criterion) {
				_where_select = ((Criterion) param).toSQL();
				_filed_select = sql_select_filed(clazz);
			}else if(param instanceof Map<?, ?>){
				Criterion criterion = (Criterion) ((Map<?, ?>) param).get("criterion");
				_where_select = criterion.toSQL();
				_filed_select = sql_select_filed(clazz, ((Map<?, ?>) param).get("fields"));
			}else{
				_filed_select = sql_select_filed(clazz);
			}
		}
		return String.format(_sql_format, _filed_select, _table_name, _where_select);
	}
	
	public static String createSqlSelectConditionCount(Class<?> clazz, Object criterion) {
		String _sql_format = "SELECT COUNT(*) FROM %s %s";
		String _table_name = sql_dbtable_name(clazz);
		String _where_select = "";

		if (criterion != null) {
			if (criterion instanceof Criterion) {
				_where_select = ((Criterion) criterion).toSQL();
			}
		}
		return String.format(_sql_format, _table_name, _where_select);
	}
	
	public static String createSqlExcute(Class<?> clazz, Object parameterObject) {
		if (parameterObject != null) {
			return (String) ((Map<?, ?>) parameterObject).get("sql");
		}
		return "error sql";
	}
	
	public static String createSqlSelectConditionPaging(Class<?> clazz, Object mapObject) {
		String _sql_format = "SELECT %s FROM %s %s";
		String _filed_select = sql_select_filed(clazz);
		String _table_name = sql_dbtable_name(clazz);
		String _where_select = "";
		if (mapObject != null) {
			if (mapObject instanceof Map) {
				MapperMethod.ParamMap<?> parameterMap = (MapperMethod.ParamMap<?>) mapObject;
				Criterion _criterion = (Criterion) parameterMap.get("param1");
				_where_select = _criterion==null?"":_criterion.toSQL();
			}
		}
		return String.format(_sql_format, _filed_select, _table_name, _where_select);
	}
	
	public static String createSqlSelectCount(String sql) {
		String _sql_format = "SELECT COUNT(1) FROM (%s) _tbl_";
		return String.format(_sql_format, removeLastOrderBys(sql));
	}
	
	/*----------------------------------------------创建查询sql-end------------------------------------------------------------------*/
	
	/*----------------------------------------------创建查询条件参数-start------------------------------------------------------------------*/
	
	public static List<Parameter> createParamsSelectAll() {
		return Lists.newArrayList();
	}
	
	public static List<Parameter> createParamsSelectAllCount() {
		return Lists.newArrayList();
	}
	
	public static List<Parameter> createParamsSelectFieldsId(Class<?> clazz, Object fields) {
		MapperMethod.ParamMap<?> fieldMap = (MapperMethod.ParamMap<?>)fields;
		return createParamsSelectId(clazz, fieldMap.get("id"));
	}
	
	public static List<Parameter> createParamsSelectId(Class<?> clazz, Object id) {
		List<Parameter> paramObjects = Lists.newArrayList();
		Field[] fields = getFields(clazz);
		for (Field field : fields) {
			field.setAccessible(true);
			try {
				if (field.isAnnotationPresent(JPA_ID) && !field.isAnnotationPresent(JPA_TRANSIENT)) {
					paramObjects.add(new Parameter(field.getName(), id));
				}
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			}
		}
		return paramObjects;
	}
	
	public static List<Parameter> createParamsSelectIds(Class<?> clazz, Object map) {
		List<Parameter> paramObjects = Lists.newArrayList();
		List<?> listIds = (List<?>) ((Map<?, ?>) map).get("list");
		for(int i = 0; i < listIds.size(); i++){
			paramObjects.add(new Parameter("list[" + i + "]", listIds.get(i)));
		}
		return paramObjects;
	}
	
	public static List<Parameter> createParamsSelectCondition(Class<?> clazz, Object param) {
		List<Parameter> paramObjects = Lists.newArrayList();
		List<Object> params = Lists.newArrayList();
		if (param instanceof Criterion) {
			params = ((Criterion) param).getParams();
		}else if(param instanceof Map<?, ?>){
			Criterion criterion = (Criterion) ((Map<?, ?>) param).get("criterion");
			params = criterion.getParams();
		}
		for (int i = 0; i < params.size(); i++) {
			paramObjects.add(new Parameter("params[" + i + "]", params.get(i)));
		}
		return paramObjects;
	}
	
	public static List<Parameter> createParamsSelectConditionCount(Class<?> clazz, Object criterion) {
		return createParamsSelectCondition(clazz, criterion);
	}
	
	public static List<Parameter> createParamsExcute(Class<?> clazz, Object map) {
		List<Parameter> paramObjects = Lists.newArrayList();
		if (map != null) {
			List<?> listParams = (List<?>) ((Map<?, ?>) map).get("list");
			for (int i = 0; i < listParams.size(); i++) {
				paramObjects.add(new Parameter("list[" + i + "]", listParams.get(i)));
			}
		}
		return paramObjects;
	}
	
	public static List<Parameter> createParamsSelectConditionPaging(Class<?> clazz, Object map) {
		List<Parameter> paramObjects = Lists.newArrayList();
		if (map instanceof Map) {
			MapperMethod.ParamMap<?> parameterMap = (MapperMethod.ParamMap<?>) map;
			Criterion _criterion=((Criterion) parameterMap.get("param1"));
			List<Object> params = _criterion==null?Lists.newArrayList():_criterion.getParams();
			for (int i = 0; i < params.size(); i++) {
				paramObjects.add(new Parameter("param1.params[" + i + "]", params.get(i)));
			}
		}
		return paramObjects;
	}
	
	public static List<Parameter> createParamsUpdateSingle(Class<?> clazz, Object object) {
		List<Parameter> paramObjects = Lists.newArrayList();
		paramObjects.addAll(createParamsInsertSingle(clazz, object,false));
		paramObjects.addAll(createParamsSelectObjId(clazz, object));
		return paramObjects;
	}
	
	private static List<Parameter> createParamsSelectObjId(Class<?> clazz, Object object) {
		List<Parameter> paramObjects = Lists.newArrayList();
		Field[] fields = getFields(clazz);
		for (Field field : fields) {
			field.setAccessible(true);
			try {
				if (field.isAnnotationPresent(JPA_ID) && !field.isAnnotationPresent(JPA_TRANSIENT)) {
					paramObjects.add(new Parameter(field.getName(), field.get(object)));
				}
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			}catch (IllegalAccessException e) {
				e.printStackTrace();
			}
			
			
		}
		return paramObjects;
	}
	
	public static List<Parameter> createParamsUpdateNotNul(Class<?> clazz, Object object) {
		List<Parameter> paramObjects = Lists.newArrayList();
		paramObjects.addAll(createParamsUpdateNotNul(clazz, object,true));
		paramObjects.addAll(createParamsSelectObjId(clazz, object));
		return paramObjects;
	}
	
	private static List<Parameter> createParamsUpdateNotNul(Class<?> clazz, Object object,boolean bAccessible) {
		List<Parameter> paramObjects = Lists.newArrayList();
		Field[] fields = getFields(clazz);
		for (Field field : fields) {
			field.setAccessible(true);
			try {
				if (field.get(object) != null && !field.isAnnotationPresent(JPA_TRANSIENT)) {
					paramObjects.add(new Parameter(field.getName(), field.get(object)));
				}
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
		}
		return paramObjects;
	}
	
	public static List<Parameter> createParamsUpdateCondition(Class<?> clazz, Object mapObject) {
		List<Parameter> paramObjects = Lists.newArrayList();
		MapperMethod.ParamMap<?> parameterMap = (MapperMethod.ParamMap<?>) mapObject;
		
		Object _paramsObj =  parameterMap.get("param1");
		Criterion _criterion = (Criterion) parameterMap.get("param2");
		if (_paramsObj instanceof Object) {
			Field[] fields = getFields(clazz);
			for (Field field : fields) {
				field.setAccessible(true);
				try {
					if (!field.isAnnotationPresent(JPA_TRANSIENT)) {
						paramObjects.add(new Parameter("param1."+field.getName(), field.get(_paramsObj)));
					}
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				}
			}
		}
		
		if (_criterion instanceof Criterion) {
			List<Object> params = ((Criterion) _criterion).getParams();
			for (int i = 0; i < params.size(); i++) {
				paramObjects.add(new Parameter("param2.params[" + i + "]", params.get(i)));
			}
		}
		
		return paramObjects;
	}
	
	public static List<Parameter> createParamsUpdateNotNulCondition(Class<?> clazz, Object mapObject) {
		List<Parameter> paramObjects = Lists.newArrayList();
		MapperMethod.ParamMap<?> parameterMap = (MapperMethod.ParamMap<?>) mapObject;
		Object _paramsObj =  parameterMap.get("param1");
		Criterion _criterion = (Criterion) parameterMap.get("param2");
		Field[] fields = getFields(clazz);
		for (Field field : fields) {
			field.setAccessible(true);
			try {
				if (field.get(_paramsObj) != null && !field.isAnnotationPresent(JPA_TRANSIENT)) {
					paramObjects.add(new Parameter("param1."+field.getName(), field.get(_paramsObj)));
				}
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
		}
		if (_criterion instanceof Criterion) {
			List<Object> params = ((Criterion) _criterion).getParams();
			for (int i = 0; i < params.size(); i++) {
				paramObjects.add(new Parameter("param2.params[" + i + "]", params.get(i)));
			}
		}
		
		return paramObjects;
	}
	
	public static List<Parameter> createParamsDeleteId(Class<?> clazz, Object id) {
		return createParamsSelectId(clazz, id);
	}
	
	public static List<Parameter> createParamsDeleteIds(Class<?> clazz, Object map) {
		return createParamsSelectIds(clazz, map);
	}
	
	public static List<Parameter> createParamsDeleteCondition(Class<?> clazz, Object criterion) {
		return createParamsSelectCondition(clazz, criterion);
	}
	/*----------------------------------------------创建查询条件参数-end------------------------------------------------------------------*/
	
	/*----------------------------------------------创建插入参数-start------------------------------------------------------------------*/
	public static List<Parameter> createParamsInsertSingle(Class<?> clazz, Object object,boolean bInsert) {
		List<Parameter> paramObjects = Lists.newArrayList();
		Field[] fields = getFields(clazz);
		for (Field field : fields) {
			field.setAccessible(true);
			try {
				if (!field.isAnnotationPresent(JPA_TRANSIENT)) {
					String _field=field.getName();
					Object _value=field.get(object);
					String keyProperty=null;
					String keyColumn  =null;
					Class<?> javaType =null;
					String generator  =null;
					boolean generated =false;
					GenerationType strategy=null;
					GenerationOrder location=null;
					if (field.isAnnotationPresent(JPA_ID)) {
						if (field.isAnnotationPresent(JPA_COLUMN)) {
							Column column = (Column) field.getAnnotation(JPA_COLUMN);
							keyColumn = StringUtils.isEmpty(column.name()) ? field.getName() : column.name();
						}
						if (field.isAnnotationPresent(JPA_GENERATED_VALUE)&&bInsert) {
							GeneratedValue generatedValue = (GeneratedValue) field.getAnnotation(JPA_GENERATED_VALUE);
							generator=generatedValue.generator();
							strategy=generatedValue.strategy();
							location=generatedValue.location();
							javaType=field.getType();
							generated=true;
							
							//如果是uuid
							if(GenerationType.UUID.equals(strategy)){
								_value=UUID.randomUUID().toString().replace("-", "");
								field.set(object, _value);
							}
						}
						keyProperty=_field;
					}
					paramObjects.add(new Parameter(_field,_value,keyColumn,keyProperty,strategy,generator,location,generated,javaType));
				}
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
		}
		return paramObjects;
	}
	
	public static List<Parameter> createParamsRepleceSingle(Class<?> clazz, Object object) {
		return createParamsInsertSingle(clazz, object,false);
	}
	
	public static List<Parameter> createParamsInsertMultiple(Class<?> clazz, Object map) {
		List<Parameter> paramObjects = Lists.newArrayList();
		if (map != null) {
			List<?> listObject = (List<?>) ((Map<?, ?>) map).get("list");
			for (int i = 0; i < listObject.size(); i++) {
				paramObjects.addAll(createParamsInsertMultiple(clazz, listObject, i));
			}
		}
		return paramObjects;
	}
	
	private static List<Parameter> createParamsInsertMultiple(Class<?> clazz, List<?> list, Integer index) {
		List<Parameter> paramObjects = Lists.newArrayList();
		Field[] fields = getFields(clazz);
		for (Field field : fields) {
			field.setAccessible(true);
			try {
				if (!field.isAnnotationPresent(JPA_TRANSIENT)) {
					paramObjects
							.add(new Parameter("list[" + index + "]." + field.getName(), field.get(list.get(index))));
				}
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
		}
		return paramObjects;
	}
	/*----------------------------------------------创建插入参数-end------------------------------------------------------------------*/
	
	/**
	 * 获取实体类的表名
	 * 
	 * @param clazz
	 * @return
	 */
	private static String sql_dbtable_name(Class<?> clazz) {
		String key_table = clazz.getName() + ".sql_dbtable_name";
		String sql_table = sql_cache.get(key_table);
		if(StringUtils.isEmpty(sql_table)){
			if(!clazz.isAnnotationPresent(JPA_TABLE)){
				sql_table = clazz.getSimpleName();
			}else{
				Table table = (Table)clazz.getAnnotation(JPA_TABLE);
				sql_table = StringUtils.isEmpty(table.name()) ? clazz.getSimpleName() : table.name(); 
			}
			sql_cache.put(key_table, sql_table);
		}
		return sql_table;
	}
	
	/**
	 * 获取查询字段
	 * @param clazz
	 * @return
	 */
	public static String sql_select_filed(Class<?> clazz){
		return sql_select_filed(clazz, null);
	}
	
	/**
	 * 获取查询字段
	 * @param clazz
	 * @param queryArray 指定查询的类的变量名（非数据库字段名）
	 * @return
	 */
	public static String sql_select_filed(Class<?> clazz, Object fieldObj){
		StringBuilder filedBuilder = new StringBuilder("");
		Field[] fields = getFields(clazz);
		boolean isEmpty = true;
		String[] fieldArray = null;
		if(fieldObj != null){
			fieldArray = (String[]) fieldObj;
			if(fieldArray.length != 0){
				isEmpty = false;
			}
		}
		for(Field field : fields){
			//这个方法很弱智，判断数组为空本来一次就够了，这里却循环一次判断一次，暂时先这样，后续看能不能优化
			if(!isEmpty){
				if(!Arrays.asList(fieldArray).contains(field.getName())){
					continue;
				}
			}
			if(!field.isAnnotationPresent(JPA_TRANSIENT)){
				if (field.isAnnotationPresent(JPA_COLUMN)) {
					Column column = (Column) field.getAnnotation(JPA_COLUMN);
					String escape = column.escape() ? "`" : "";
					String columnStr = StringUtils.isEmpty(column.name()) ? "" : escape + column.name() + escape;
					filedBuilder.append(columnStr).append(",");
				}else{
					filedBuilder.append(field.getName() + ",");
				}
			}
		}
		return filedBuilder.length() == 0 ? "" : filedBuilder.substring(0, filedBuilder.length() - 1);
	}
	
	private static String sql_select_where_id(Class<?> clazz) {
		StringBuilder whereBulider = new StringBuilder("");
		Field[] fields = getFields(clazz);
		for (Field field : fields) {
			field.setAccessible(true);
			if (field.isAnnotationPresent(JPA_ID)) {
				if (field.isAnnotationPresent(JPA_COLUMN)) {
					Column column = (Column) field.getAnnotation(JPA_COLUMN);
					String _escape = column.escape() ? "`" : "";
					String _column = StringUtils.isEmpty(column.name()) ? field.getName() : column.name();
					whereBulider.append(_escape).append(_column).append(_escape).append(" = ? AND ");
				} else {
					whereBulider.append(field.getName()).append(" = ? AND ");
				}
			}
		}
		return whereBulider.length() == 0 ? "" : whereBulider.substring(0, whereBulider.length() - 4);
	}
	
	private static String sql_insert_filed(Class<?> clazz) {
		StringBuilder filedBuilder = new StringBuilder("");
		Field[] fields = getFields(clazz);
		for (Field field : fields) {
			field.setAccessible(true);
			if (!field.isAnnotationPresent(JPA_TRANSIENT)) {
				if (field.isAnnotationPresent(JPA_COLUMN)) {
					Column column = (Column) field.getAnnotation(JPA_COLUMN);
					String _escape = column.escape() ? "`" : "";
					String _column = StringUtils.isEmpty(column.name()) ? field.getName() : column.name();
					filedBuilder.append(_escape).append(_column).append(_escape).append(",");
				} else {
					filedBuilder.append(field.getName()).append(",");
				}
			}
		}
		return filedBuilder.length() == 0 ? "" : filedBuilder.substring(0, filedBuilder.length() - 1);
	}
	
	private static String sql_insert_value(Class<?> clazz) {
		StringBuilder paramBuilder = new StringBuilder("");
		Field[] fields = getFields(clazz);
		for (Field field : fields) {
			field.setAccessible(true);
			if (!field.isAnnotationPresent(JPA_TRANSIENT)) {
				paramBuilder.append("?,");
			}
		}
		return paramBuilder.length() == 0 ? "" : paramBuilder.substring(0, paramBuilder.length() - 1);
	}
	
	/**
	 * 更新所有字段
	 * 
	 * @param clazz
	 * @return
	 */
	private static String sql_update_filed(Class<?> clazz) {
		StringBuilder updateBuilder = new StringBuilder("");
		Field[] fields = getFields(clazz);
		for (Field field : fields) {
			field.setAccessible(true);
			if (!field.isAnnotationPresent(JPA_TRANSIENT)) {
				if (field.isAnnotationPresent(JPA_COLUMN)) {
					Column column = (Column) field.getAnnotation(JPA_COLUMN);
					String _escape = column.escape() ? "`" : "";
					String _column = StringUtils.isEmpty(column.name()) ? field.getName() : column.name();
					updateBuilder.append(_escape).append(_column).append(_escape).append("=?,");
				} else {
					updateBuilder.append(field.getName()).append("=?,");
				}
			}
		}
		return updateBuilder.length() == 0 ? "" : updateBuilder.substring(0, updateBuilder.length() - 1);
	}
	
	/**
	 * 更新非空字段
	 * 
	 * @param clazz
	 * @param object
	 * @return
	 */
	private static String sql_update_filed(Class<?> clazz, Object object) {
		StringBuilder updateBuilder = new StringBuilder("");
		Field[] fields = getFields(clazz);
		for (Field field : fields) {
			field.setAccessible(true);
			if (!field.isAnnotationPresent(JPA_TRANSIENT)) {
				try {
					Object value = field.get(object);
					if (value == null) {
                        continue;
                    }
					if (field.isAnnotationPresent(JPA_COLUMN)) {
						Column column = (Column) field.getAnnotation(JPA_COLUMN);
						String _escape = column.escape() ? "`" : "";
						String _column = StringUtils.isEmpty(column.name()) ? field.getName() : column.name();
						updateBuilder.append(_escape).append(_column).append(_escape).append("=?,");
					} else {
						updateBuilder.append(field.getName()).append("=?,");
					}
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				}
			}
		}
		return updateBuilder.length() == 0 ? "" : updateBuilder.substring(0, updateBuilder.length() - 1);
	}
	
	private static String removeLastOrderBys(String sql) {
		int iEnd =SqlExecutorUtil.removeBreakingWhitespace(sql).toUpperCase().lastIndexOf("ORDER BY");
		return iEnd == -1 ? sql : sql.substring(0, iEnd);
	}

	private static Field[] getFields(Class clazz){
		Field[] fields = clazz.getDeclaredFields();
		if(clazz.getSuperclass() != Object.class){
			Field[] pFields = clazz.getSuperclass().getDeclaredFields();
			fields = ArrayUtils.addAll(fields, pFields);
		}
		return  fields;
	}

}
