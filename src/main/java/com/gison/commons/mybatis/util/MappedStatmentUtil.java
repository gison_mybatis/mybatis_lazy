package com.gison.commons.mybatis.util;

import com.gison.commons.mybatis.dialect.IDialect;
import com.gison.commons.mybatis.enmu.GenerationOrder;
import com.gison.commons.mybatis.enmu.GenerationType;
import com.gison.commons.mybatis.query.Parameter;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.builder.StaticSqlSource;
import org.apache.ibatis.executor.keygen.Jdbc3KeyGenerator;
import org.apache.ibatis.executor.keygen.NoKeyGenerator;
import org.apache.ibatis.executor.keygen.SelectKeyGenerator;
import org.apache.ibatis.logging.Log;
import org.apache.ibatis.logging.LogFactory;
import org.apache.ibatis.mapping.*;
import org.apache.ibatis.scripting.defaults.RawSqlSource;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.RowBounds;
import org.apache.ibatis.type.ObjectTypeHandler;

import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class MappedStatmentUtil {
	protected static Log log = LogFactory.getLog(MappedStatmentUtil.class);
	public static Map<String, Class<?>> clazzs_cache = Maps.newConcurrentMap();
	
	/**
	 * 根据方法名拿到接口的泛型，并转化成实体类
	 * @param selectId
	 * @return
	 */
	public static Class<?> transResourceToEntityClass(String selectId) {
		Class<?> clazz = clazzs_cache.get(selectId);
        try {
        	if(clazz == null){
        		clazz = Class.forName(selectId.substring(0, selectId.lastIndexOf(".")));
    			clazz = (Class<?>) ((ParameterizedType) clazz.getGenericInterfaces()[0])
    					.getActualTypeArguments()[0];	
    			clazzs_cache.put(selectId, clazz);
        	}
        	
		} catch (Exception e) {
			log.error("泛型-->类出错：", e);
			e.printStackTrace();
		} 
        return clazz;
	}
	
	public static MappedStatement createGenericMappedStatement(Class<?> clazz, MappedStatement mappedStatement,
															   String sourceSql, Object parameterObject, List<Parameter> parameters, IDialect dialect) {
		SqlObject _sqlObj = createGenericSqlObject(mappedStatement, sourceSql, parameterObject, parameters, clazz);
		return createMappedStatement(clazz, _sqlObj, dialect);
	}
	
	public static MappedStatement createPaginationMappedStatement(MappedStatement mappedStatement, String sourceSql,
			Object parameterObject, List<ParameterMapping> parameterMappings, RowBounds rowBounds, IDialect dialect) {
		String _sourceSql = dialect.createPagingSql(sourceSql, rowBounds.getOffset(), rowBounds.getLimit());
		SqlObject _sqlObj = createPaginationSqlObject(mappedStatement, _sourceSql, parameterObject, parameterMappings);
		return createMappedStatement(null, _sqlObj);
	}
	
	public static SqlObject createPaginationSqlObject(MappedStatement mappedStatement, String sourceSql,
			Object parameterObject, List<ParameterMapping> parameterMappings) {
		return new SqlObject(sourceSql, parameterObject, mappedStatement, parameterMappings);
	}
	
	public static SqlObject createGenericSqlObject(MappedStatement mappedStatement, String sourceSql,
			Object parameterObject, List<Parameter> parameters, Class<?> clazz) {
		Configuration configuration = mappedStatement.getConfiguration();
		List<ParameterMapping> parameterMappings = Lists.newArrayList();
		KeysObject keysObject = new KeysObject();

		for (Parameter parameter : parameters) {
			ParameterMapping.Builder builder = null;
			if (parameter.getValue() != null) {
				builder = new ParameterMapping.Builder(configuration, parameter.getField(), parameter.getValue()
						.getClass());
			} else {
				builder = new ParameterMapping.Builder(configuration, parameter.getField(), new ObjectTypeHandler());
			}
			parameterMappings.add(builder.build());
			keysObject.keyColumn(parameter.getKeyColumn());
			if(keysObject.keyProperty(parameter.getKeyProperty())){
				keysObject.javaType(parameter.getJavaType());
			}
			
			if(keysObject.isGenerated()==false){
				keysObject.setPrepose(!GenerationOrder.AFTER.equals(parameter.getLocation()));
				keysObject.setStrategy(parameter.getStrategy());
			}
			
			keysObject.setGenerator(parameter.getGenerator());
			keysObject.setGenerated(parameter.isGenerated()); // 变成真后 赋值无效
		}

		return new SqlObject(sourceSql, parameterObject, keysObject, mappedStatement, parameterMappings);
	}
	
	public static MappedStatement createMappedStatement(Class<?> clazz, SqlObject sqlObject) {
		return createMappedStatement(clazz,sqlObject, null);
	}

	public static MappedStatement createMappedStatement(Class<?> clazz, SqlObject sqlObject, IDialect dialect) {
		MappedStatement mappedStatement = sqlObject.getMappedStatement();
		Configuration configuration = mappedStatement.getConfiguration();
		List<ParameterMapping> parameterMappings = sqlObject.getParameterMappings();
		StaticSqlSource staticSqlSource = new StaticSqlSource(configuration, sqlObject.getSql(), parameterMappings);

		MappedStatement.Builder builder = new MappedStatement.Builder(configuration, mappedStatement.getId(),
				staticSqlSource, mappedStatement.getSqlCommandType());
		builder.fetchSize(mappedStatement.getFetchSize());
		builder.flushCacheRequired(mappedStatement.isFlushCacheRequired());
		builder.resultOrdered(mappedStatement.isResultOrdered());
		builder.cache(mappedStatement.getCache());
		builder.useCache(mappedStatement.isUseCache());
		builder.lang(mappedStatement.getLang());
		builder.parameterMap(mappedStatement.getParameterMap());
		builder.resource(mappedStatement.getResource());

		List<ResultMap> resultMaps = mappedStatement.getResultMaps();

		if (clazz != null) {
			Class<?> typeClazz = mappedStatement.getResultMaps().get(0).getType();
			if (typeClazz.equals(Object.class) || typeClazz.equals(List.class)) {
				String mappedStatementId = mappedStatement.getId();
				String resultMapId = mappedStatementId.substring(0, mappedStatementId.lastIndexOf("."))
						+ ".GenericObject";
				ResultMap.Builder resultMapBuilder = new ResultMap.Builder(configuration, resultMapId, clazz,
						new ArrayList<ResultMapping>());
				resultMaps = Lists.newArrayList(resultMapBuilder.build());
			}
		}
		builder.resultMaps(resultMaps);
		builder.resultSetType(mappedStatement.getResultSetType());
		if (mappedStatement.getResultSets() != null) {
			for (String resulSets : mappedStatement.getResultSets()) {
				builder.resultSets(resulSets);
			}
		}
		builder.statementType(mappedStatement.getStatementType());
		builder.databaseId(mappedStatement.getDatabaseId());
		builder.timeout(mappedStatement.getTimeout());

		//
		if (mappedStatement.getSqlCommandType() == SqlCommandType.INSERT) {
			KeysObject keysObject=sqlObject.getKeysObject();
			if(keysObject.isGenerated()){
				for (String keyProperty : keysObject.keyProperties()) {
					builder.keyProperty(keyProperty);
				}
				
				for (String keyColumn : keysObject.keyColumns()) {
					builder.keyColumn(keyColumn);
				}
				
				
				if(GenerationType.JDBC.equals(keysObject.getStrategy())){ //mysql 自增
					builder.keyGenerator(new Jdbc3KeyGenerator());
				}else if(GenerationType.UUID.equals(keysObject.getStrategy())){ //mysql UUID
					
				}else{
					log.debug("暂未完成开发 其它数据库");
					String keyId=mappedStatement.getId() + SelectKeyGenerator.SELECT_KEY_SUFFIX;
					if(configuration.getMappedStatementNames().contains(keyId)==false){
						MappedStatement selectKeyMappedStatment=createSelectKeyMappedStatment(mappedStatement,keysObject.keyProperties(),keysObject.getGenerator(),clazz);
						configuration.addMappedStatement(selectKeyMappedStatment);
					}
					MappedStatement keyStatement=configuration.getMappedStatement(keyId,true);
					SelectKeyGenerator keyGenerator=new SelectKeyGenerator(keyStatement,keysObject.isPrepose());
					builder.keyGenerator(keyGenerator);
				}
			}
		}
		
		return builder.build();
	}
	
	public static MappedStatement createSelectKeyMappedStatment(MappedStatement mappedStatement,
			List<String> properties, String selectKeySql, Class<?> clazz) {
		String keyId = mappedStatement.getId() + SelectKeyGenerator.SELECT_KEY_SUFFIX;
		Configuration configuration = mappedStatement.getConfiguration();
		SqlSource sqlSource = new RawSqlSource(configuration, selectKeySql, clazz);

		MappedStatement.Builder builder = new MappedStatement.Builder(configuration, keyId, sqlSource,
				SqlCommandType.SELECT);
		builder.resource(mappedStatement.getResource());
		builder.fetchSize(null);
		builder.statementType(StatementType.STATEMENT);
		builder.keyGenerator(new NoKeyGenerator());
		for (String property : properties) {
			builder.keyProperty(property);
		}

		builder.keyColumn(null);
		builder.databaseId(null);
		builder.lang(configuration.getDefaultScriptingLanguageInstance());
		builder.resultOrdered(false);
		builder.resultSets(null);
		builder.timeout(configuration.getDefaultStatementTimeout());

		List<ParameterMapping> parameterMappings = new ArrayList<ParameterMapping>();
		ParameterMap.Builder inlineParameterMapBuilder = new ParameterMap.Builder(configuration, builder.id()
				+ "-Inline", clazz, parameterMappings);
		builder.parameterMap(inlineParameterMapBuilder.build());

		List<ResultMap> resultMaps = new ArrayList<ResultMap>();
		ResultMap.Builder inlineResultMapBuilder = new ResultMap.Builder(configuration, builder.id() + "-Inline",
				Integer.class, new ArrayList<ResultMapping>());
		
		resultMaps.add(inlineResultMapBuilder.build());
		builder.resultMaps(resultMaps);
		builder.resultSetType(null);

		builder.flushCacheRequired(false);
		builder.useCache(false);
		builder.cache(null);

		return builder.build();

	}
}

class SqlObject {
	private String sql;
	private KeysObject keysObject;
	private Object parameterObject;
	private MappedStatement mappedStatement;
	private List<ParameterMapping> parameterMappings;

	public SqlObject() {

	}

	public SqlObject(String sql, Object parameterObject, MappedStatement mappedStatement,
			List<ParameterMapping> parameterMappings) {
		this.sql = sql;
		this.parameterObject = parameterObject;
		this.mappedStatement = mappedStatement;
		this.parameterMappings = parameterMappings;
	}

	public SqlObject(String sql, Object parameterObject, KeysObject keysObject, MappedStatement mappedStatement,
			List<ParameterMapping> parameterMappings) {
		this.sql = sql;
		this.keysObject = keysObject;
		this.parameterObject = parameterObject;
		this.mappedStatement = mappedStatement;
		this.parameterMappings = parameterMappings;
	}

	public String getSql() {
		return sql;
	}

	public void setSql(String sql) {
		this.sql = sql;
	}

	public KeysObject getKeysObject() {
		return keysObject;
	}

	public void setKeysObject(KeysObject keysObject) {
		this.keysObject = keysObject;
	}

	public Object getParameterObject() {
		return parameterObject;
	}

	public void setParameterObject(Object parameterObject) {
		this.parameterObject = parameterObject;
	}

	public List<ParameterMapping> getParameterMappings() {
		return parameterMappings;
	}

	public void setParameterMappings(List<ParameterMapping> parameterMappings) {
		this.parameterMappings = parameterMappings;
	}

	public MappedStatement getMappedStatement() {
		return mappedStatement;
	}

	public void setMappedStatement(MappedStatement mappedStatement) {
		this.mappedStatement = mappedStatement;
	}

}

class KeysObject {
	private List<String> keyProperties = Lists.newArrayList();
	private List<String> keyColumns = Lists.newArrayList();
	private List<Class<?>> javaTypes = Lists.newArrayList();
	private String generator;
	private boolean prepose;
	private boolean generated;
	private GenerationType strategy;
	
	
	public GenerationType getStrategy() {
		return strategy;
	}

	public void setStrategy(GenerationType strategy) {
		this.strategy = strategy;
	}

	public boolean isGenerated() {
		return generated;
	}

	public void setGenerated(boolean generated) {
		if (this.generated == false) {
			this.generated = generated;
		}
	}

	public boolean isPrepose() {
		return prepose;
	}

	public void setPrepose(boolean prepose) {
		this.prepose = prepose;
	}

	public String getGenerator() {
		return generator;
	}

	public void setGenerator(String generator) {
		if (StringUtils.isEmpty(this.generator)) {
			this.generator = generator;
		}
	}

	public List<Class<?>> javaTypes(){
		return javaTypes;
	}
	

	public void javaType(Class<?> type){
		 javaTypes.add(type);
	}
	
	public List<String> keyProperties() {
		return keyProperties;
	}

	public List<String> keyColumns() {
		return keyColumns;
	}

	public boolean keyProperty(String keyProperty) {
		if (StringUtils.isNotEmpty(keyProperty)) {
			keyProperties.add(keyProperty);
			return true;
		}
		return false;
	}
	
	

	public void keyColumn(String keyColumn) {
		if (StringUtils.isNotEmpty(keyColumn)) {
			keyColumns.add(keyColumn);
		}
	}
}
